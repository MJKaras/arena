from django.shortcuts import render
from django.http import HttpResponse
from .models import GameState
from .tic_tac_toe import *


def index(request):
    return HttpResponse("Hello, world. You're at the tic_tac_toe index.")


def player_move(request):
    game_id = int(request.GET.get('game_id'))
    player_id = int(request.GET.get('player_id'))
    x = int(request.GET.get('x'))
    y = int(request.GET.get('y'))

    game_record_list = GameState.objects.get_or_create(game_id=game_id) # get_or_create returns tuple [object, is_created]
    game_record = game_record_list[0]
    if game_record_list[1]: # new game
        game = Game()
    else: # loaded game
        json_data = game_record.game_data
        game = Game(json_data)

    if game.player_move(player_id=player_id, x=x, y=y):
        if game.winner == 0:
            game.bot_move(bot_id=player_id+1)
        json_data = game.drop_to_json()
        game_record.game_data = json_data
        game_record.save()

    response = HttpResponse()
    response.content = json_data
    return response
