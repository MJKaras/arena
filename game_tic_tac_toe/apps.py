from django.apps import AppConfig


class GameTicTacToeConfig(AppConfig):
    name = 'game_tic_tac_toe'
