# Generated by Django 2.1.3 on 2018-11-12 22:23

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='gameState',
            fields=[
                ('game_id', models.IntegerField(primary_key=True, serialize=False)),
                ('game_data', django.contrib.postgres.fields.jsonb.JSONField()),
            ],
        ),
    ]
