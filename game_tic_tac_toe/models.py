from django.db import models
from django.contrib.postgres.fields import JSONField


# Create your models here.
class GameState(models.Model):
    game_id = models.IntegerField(primary_key=True)
    game_data = JSONField(default=dict)

