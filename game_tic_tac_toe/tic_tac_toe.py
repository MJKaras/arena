import json
from random import randint

class Game:
    def __init__(self, json_source=None):
        if json_source: # load game
            loaded_json = json.loads(json_source)
            self.board = loaded_json['board']
            self.random_seed = loaded_json['random_seed']
            self.winner = loaded_json['winner']
            self.game_id = loaded_json['game_id']
            self.move_count = loaded_json['move_count']
        else: # new game
            self.board = [[0 for col in range(3)] for row in range(3)]  # 3x3 array of zeros
            self.random_seed = 0
            self.winner = 0
            self.game_id = 0
            self.move_count=0

    def player_move(self, player_id, x, y):
        if self.board[x][y] == 0:
            self.board[x][y] = player_id
            self.move_count += 1
            if check_for_win(self.board, x, y):
                self.winner = player_id
            return True
        else:
            return False

    def bot_move(self, bot_id=2):
        if self.move_count < 9:
            x = randint(0, 2)
            y = randint(0, 2)
            while self.board[x][y] != 0:
                x = randint(0, 2)
                y = randint(0, 2)
            self.player_move(bot_id, x, y)

    def drop_to_json(self):
        json_dump = json.dumps(self, default=lambda x: x.__dict__)
        return json_dump

def check_for_win(board, x, y):
    # check horizontal win
    if board[0][y] == board[1][y] == board[2][y]:
        return True

    # check vertical win
    if board[x][0] == board[x][1] == board[x][2]:
        return True

    # check 1st diagonal win
    if board[0][0] == board[1][1] == board[2][2] != 0:
        return True

    # check 2nd diagonal win
    if board[2][0] == board[1][1] == board[0][2] != 0:
        return True
