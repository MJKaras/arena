from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('player_move', views.player_move, name='player_move'),
]